const express = require("express");
const morgan = require('morgan');
const engine = require("ejs-mate");
const app = express();

app.engine("ejs",engine);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.set("PORT", process.env.PORT|3000);

app.use(morgan("dev"));
app.use(express.json());

module.exports = app;