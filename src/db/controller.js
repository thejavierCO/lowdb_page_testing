const dbcfg = require("../config");
const db = new dbcfg("tasks");
db.init();

const getValue = (req,res,next)=>{
    res.json(db.get(req.params.id));
}
const getValues = (req,res,next)=>{
    res.json(db.get());
}
const setValue = (req,res,next)=>{
    res.json(db.set(req.body));
}
const delValue = async (req,res,next)=>{
    res.json(await db.del(req.params.id));
}
const updateValue = async (req,res,next)=>{
    res.json(await db.del(req.body,req.params.id));
}

module.exports = {
    getValue,
    getValues,
    setValue,
    delValue,
    updateValue
}