const low = require("lowdb");
const fileasync = require("lowdb/adapters/FileAsync");
const { v4 } = require("uuid");

class db{
    constructor(name){
        this.adapter = new fileasync(name+".json");
        this.name = name;
    }
    async init(){
        let data = {};
        data[this.name] = []
        this.db = await low(this.adapter);
        this.db.defaults(data).write();
    }
    get(data){
        let result;
        if(data){
            result = this.db.get(this.name).find({id:data}).value();
        }else{
            result = this.db.get(this.name).value();
        }
        return result;
    }
    set(data){
        data.id = v4();
        this.db.get(this.name).push(data).write()
        return data;
    }
    async update(data,id){
        let result = await this.db
        .get(this.name)
        .find({id:id})
        .assign(data)
        .write()
        return result;
    }
    async del(id){
        let result = await this.db
        .get(this.name)
        .remove({id:id})
        .write()
        return result;
    }
}

module.exports = db;